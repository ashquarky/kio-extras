# translation of kio_fish.po to Persian
# Nazanin Kazemi <kazemi@itland.ir>, 2006.
# MaryamSadat Razavi <razavi@itland.ir>, 2006.
# Nasim Daniarzadeh <daniarzadeh@itland.ir>, 2006.
# Mohammad Reza Mirdamadi <mohi@linuxshop.ir>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: kio_fish\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-11 00:46+0000\n"
"PO-Revision-Date: 2013-01-10 22:39+0330\n"
"Last-Translator: Mohammad Reza Mirdamadi <mohi@linuxshop.ir>\n"
"Language-Team: Farsi (Persian) <kde-i18n-fa@kde.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.5\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: fish.cpp:322
#, kde-format
msgid "Connecting..."
msgstr "اتصال..."

#: fish.cpp:656
#, kde-format
msgid "Initiating protocol..."
msgstr "آغاز قرارداد..."

#: fish.cpp:693
#, kde-format
msgid "Local Login"
msgstr "ورود محلی"

#: fish.cpp:695
#, kde-format
msgid "SSH Authentication"
msgstr "احراز هویت ssh"

#: fish.cpp:732 fish.cpp:747
#, kde-format
msgctxt "@action:button"
msgid "Yes"
msgstr ""

#: fish.cpp:732 fish.cpp:747
#, kde-format
msgctxt "@action:button"
msgid "No"
msgstr ""

#: fish.cpp:830
#, kde-format
msgid "Disconnected."
msgstr "قطع ارتباط."
